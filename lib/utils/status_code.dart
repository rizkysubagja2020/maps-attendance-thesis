// this class represent for definded status code
class StatusCode {
  static const appCodeUserInvalid = 1000;
  static const appCodeTokenInvalid = 1001;
  static const appCodeAuthInvalid = 1002;
  static const appCodeValidateData = 1003;
  static const appCodeFailedSendingEmail = 1004;
  static const appCodeFailedSavedData = 1005;
  static const appCodeRequiredField = 1006;
  static const appCodeDeviceInvalid = 1007;
  static const appCodeFailedGetData = 1008;
  static const appCodeEmptyBody = 1011;

  // Error code commonly used;
  static const codeOk = 200;
  static const codeUnknownAction = 301;
  static const codeParamEmpty = 302;
  static const codeBadRequest = 400;
  static const codeUnauthorized = 401;
  static const codePaymentRequired = 402;
  static const codeForbidden = 403;
  static const codeNotFound = 404;
  static const codeIntServerError = 500;
  static const codeNotImplemented = 501;
}
