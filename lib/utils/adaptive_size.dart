import 'package:get/get.dart';

class ResponsiveSize {
  ResponsiveSize._();

  static double width(double percentage) {
    final value = Get.width * percentage / 100;

    return value;
  }

  static double height(double percentage) {
    final value = Get.height * percentage / 100;

    return value;
  }
}
