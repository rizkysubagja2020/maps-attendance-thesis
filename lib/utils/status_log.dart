// this class represent log category
class StatusLog {
  static const logNetworking = "NETWORKING";
  static const logResponse = "RESPONSE";
  static const logEndPoint = "ENDPOINT";
  static const logHeader = "HEADER";
  static const logParameter = "PARAMETER";
  static const logIntercept = "INTERCEPTOR";
}
