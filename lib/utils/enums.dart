enum AttendanceType { clockin, clockout }

enum LocationType { inside, outside, none }

enum DayType { workDay, offDay }

enum EventType { officeEvent, nationalEvent }

enum ButtonClockType {
  buttonWillClockIn,
  buttonClockin,
  buttonWillClockOut,
  buttonClockOut,
  buttonDayOff,
  buttonPlaceholder,
}

enum LocationPermissionType {
  servicesDisable,
  locationDenied,
  locationDisable,
  locationEnable
}

enum NetworkStatus { online, offline }

enum ValidateIndentifier { validateForgotPassword, validateLoginUser }

enum ImgType { imgTypeNetwork, imgTypeFile }

enum Navigate { next, prev }

enum SnackbarType { error, success, info, custom }

enum BrightnessType { light, dark, danger, disable, optional }

enum AlignTextType { left, center, right }

// enum identifier for endpoint
enum EndPointName {
  // auth keywords
  authorize,
  accessToken,
  refreshToken,
  // app
  appInfo,
  // user
  userInfo,
  userLogout,
  userChangePassword,
  // attenance
  attendanceInfo,
  attendanceCalendar,
  attendanceHistory,
  attendanceType,
  // error keywords
  testUnAuth,
  testBadRequest
}
