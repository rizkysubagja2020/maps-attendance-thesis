class Assets {
  Assets._();
  // icons
  static String iconBack = "assets/icons/Back.png";
  static String iconBackSpace = "assets/icons/backspace.png";
  static String iconCalendarHistory = "assets/icons/calendar_history.png";
  static String iconCalendar = "assets/icons/calendar.png";
  static String iconCamera = "assets/icons/camera.png";
  static String iconClockinButton = "assets/icons/clockin_button.png";
  static String iconClockin = "assets/icons/clockin.png";
  static String iconClockoutButton = "assets/icons/clockout_button.png";
  static String iconDashboard = "assets/icons/dashboard.png";
  static String iconEyeOff = "assets/icons/eye-off.png";
  static String iconEye = "assets/icons/eye.png";
  static String iconHistory = "assets/icons/history.png";
  static String iconHome = "assets/icons/home.png";
  static String iconImage = "assets/icons/image.png";
  static String iconInsideOffice = "assets/icons/inside_office.png";
  static String iconLogout = "assets/icons/logout.png";
  static String iconMapMarker = "assets/icons/map_marker.png";
  static String iconMapMarkerRed = "assets/icons/map_marker_red.png";
  static String iconMenu = "assets/icons/menu.png";
  static String iconMyLocation = "assets/icons/current_location.png";
  static String iconOutSideOffice = "assets/icons/outside_office.png";
  static String iconLockKey = "assets/icons/password.png";
  static String iconSelfie = "assets/icons/selfie.png";
  static String iconUser = "assets/icons/user.png";

  // images
  static String imageCheck = "assets/images/Check.png";
  static String imageForgotPassword = "assets/images/forgot_password.png";
  static String imageLogout = "assets/images/logout.png";
  static String imageUser = "assets/images/user.png";
  static String imageDRTWhite = "assets/images/drt-white.png";
  static String imageDRTGreen = "assets/images/drt-green.png";
  static String imageLogoApp = "assets/images/logo-app.png";
  static String imageNoEvents = "assets/images/no_events.png";
  static String imageSplashScreen = "assets/images/splash_screen.png";
}
