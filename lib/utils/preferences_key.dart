// this class represent for shared preferences key
class PreferencesKey {
  static const keyAccessToken = "access_token";
  static const keyRefreshToken = "refresh_token";
  static const keyCountry = "country";
  static const keyLanguage = "language";
  static const keyUserProfileName = "full_name";
  static const keyChangePassword = "is_change_password";
  static const keyChangePasswordMessage = "change_password_message";
}
