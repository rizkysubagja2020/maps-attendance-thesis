import 'package:flutter/material.dart';

class AppColors {
  Color _hex({required String colorCode}) {
    final String _containHex = colorCode.toUpperCase().replaceAll("#", "");
    String _result = "";
    if (colorCode.length == 7) {
      _result = "FF$_containHex";
    }

    return Color(int.parse(_result, radix: 16));
  }

  // list of custom colors
  static Color greenDark = AppColors()._hex(colorCode: "#3bc0bd");
  static Color greenLight = AppColors()._hex(colorCode: "#2de9e4");
  static Color greenMiddle = AppColors()._hex(colorCode: "#3adbd7");
  static Color bluePrimary = AppColors()._hex(colorCode: "#3B78C0");
  static Color redDayOff = AppColors()._hex(colorCode: "#C03B3B");
  static Color redPinPoint = AppColors()._hex(colorCode: "#C91C1C");
  static Color redAlert = AppColors()._hex(colorCode: "#EB6B62");
  static Color yellowEvent = AppColors()._hex(colorCode: "#EECC19");
  static Color greyDisabled = AppColors()._hex(colorCode: "#DADADA");
  static Color whiteSkeleton = AppColors()._hex(colorCode: "#EFEFEF");
  static Color whiteBackground = AppColors()._hex(colorCode: "#FFFFFF");
  static Color basicDark = AppColors()._hex(colorCode: "#2D2D2D");
  static Color lightDart = AppColors()._hex(colorCode: "#999999");
  static Color greenMapBoundary =
      AppColors()._hex(colorCode: "#3bc0bd").withOpacity(0.20);
  static Color rippleColor =
      AppColors()._hex(colorCode: "#EFEFEF").withOpacity(0.20);
}
