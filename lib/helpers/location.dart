import 'dart:developer';

import 'package:geolocator/geolocator.dart';

import 'permission_handler.dart';

class Location {
  Future<List<double>> getCurrentLocation() async {
    final bool isGranted = await PermissionHandler.locationPermission();

    log("CHECK IS GRANTED GPS = $isGranted");

    if (isGranted) {
      Position? position = await Geolocator.getLastKnownPosition();
      position = await Geolocator.getCurrentPosition();

      log("latitude = ${position.latitude}");
      log("latitude = ${position.longitude}");

      return [position.latitude, position.longitude];
    } else {
      return [0.0, 0.0];
    }
  }
}
