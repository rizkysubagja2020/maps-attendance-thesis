import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';

import 'shared_pref.dart';

dynamic ellipsisName(String paramName) async {
  final SharedPref _sharedPref = SharedPref();
  String _ellipsisNameUser = "";

  if (paramName.length >= 19) {
    _ellipsisNameUser = paramName.substring(0, 17);
  }
  if (paramName.length < 19) {
    await _sharedPref.writeFullName(paramName);
  } else {
    await _sharedPref.writeFullName("$_ellipsisNameUser...");
  }
}

/// this method receive Map data and will return to string url-encoded
String urlQueries(Map data) {
  final List queries = [];
  data.forEach((key, value) {
    final String name = "$key=$value";
    queries.add(name);
  });
  final String stringQueries = queries.join("&");
  return stringQueries;
}

String clockInDateTimeModify(String param) {
  if (param == "-") {
    return param;
  } else {
    return DateFormat("HH:mm").format(DateTime.parse(param));
  }
}

String clockOutDateTimeModify(String param) {
  if (param == "-") {
    return param;
  } else {
    return DateFormat("HH:mm").format(
      DateTime.parse(param),
    );
  }
}

LocationType clockInTypeModify(String param) {
  if (param == "Outside Office") {
    return LocationType.outside;
  } else if (param == "Inside Office") {
    return LocationType.inside;
  } else {
    return LocationType.none;
  }
}

LocationType clockOutTypeModify(String param) {
  if (param == "Outside Office") {
    return LocationType.outside;
  } else if (param == "Inside Office") {
    return LocationType.inside;
  } else {
    return LocationType.none;
  }
}

String getDateCreatedSheet(String param) {
  if (param.isNotEmpty) {
    return DateFormat("EEEE, dd MMMM y").format(DateTime.parse(param));
  } else {
    return "";
  }
}

String getNameProfile(Map param) {
  if (param["name"] == null) {
    return "";
  } else {
    return param["name"].toString();
  }
}

String getNikProfile(Map param) {
  if (param["nik"] == null) {
    return "";
  } else {
    return param["nik"].toString();
  }
}

String getGenderProfile(Map param) {
  if (param["gender"] == null) {
    return "";
  } else {
    return param["gender"].toString();
  }
}

String getReligionProfile(Map param) {
  if (param["religion"] == null) {
    return "";
  } else {
    return param["religion"].toString();
  }
}

String getBirthPlaceProfile(Map param) {
  if (param["birth_place"] == null) {
    return "";
  } else {
    return param["birth_place"].toString();
  }
}

String getBirthDateProfile(Map param) {
  if (param["birth_date"] == null) {
    return "";
  } else {
    return DateFormat("dd MMMM y").format(
      DateTime.parse(
        param["birth_date"].toString(),
      ),
    );
  }
}

String getAgeProfile(Map param) {
  if (param["age"] == null) {
    return "";
  } else {
    return param["age"].toString();
  }
}

String getPersonalEmailProfile(Map param) {
  if (param["email_personal"] == null) {
    return "";
  } else {
    return param["email_personal"].toString();
  }
}

String getOfficeEmailProfile(Map param) {
  if (param["email_office"] == null) {
    return "";
  } else {
    return param["email_office"].toString();
  }
}

String getPhoneNumberProfile(Map param) {
  if (param["phone"] == null) {
    return "";
  } else {
    return param["phone"].toString();
  }
}

String getAdressProfile(Map param) {
  if (param.isEmpty) {
    return "";
  } else {
    return "${param["address_now"]}, ${param["city"]}, ${param["province"]}";
  }
}

String getPositionProfile(Map param) {
  if (param["positions"] == null) {
    return "";
  } else {
    final List<dynamic> _containPosition = param["positions"] as List<dynamic>;
    final StringBuffer _result = StringBuffer();
    if (_containPosition.length == 1) {
      return _containPosition[0].toString();
    } else {
      for (int index = 0; index < _containPosition.length; index++) {
        _result.write(_containPosition[index] + "\n");
      }

      return _result.toString();
    }
  }
}

String getDivisionProfile(Map param) {
  if (param["divisions"] == null) {
    return "";
  } else {
    final List<dynamic> _containDivision = param["divisions"] as List<dynamic>;
    final StringBuffer _result = StringBuffer();
    if (_containDivision.length == 1) {
      return _containDivision[0].toString();
    } else {
      for (int index = 0; index < _containDivision.length; index++) {
        _result.write(_containDivision[index] + "\n");
      }

      return _result.toString();
    }
  }
}

Color onSetColorMapMarker(LocationType locationType) {
  if (locationType == LocationType.inside ||
      locationType == LocationType.outside) {
    return AppColors.basicDark;
  } else {
    return AppColors.greyDisabled;
  }
}

Color onSetColorClock(AttendanceType type, Map clockDateTime) {
  if (type == AttendanceType.clockin) {
    if (clockDateTime["clockIn"] == "-") {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  } else {
    if (clockDateTime["clockOut"] == "-") {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  }
}

Color onSetColorNotes(
  AttendanceType type,
  Map clockNotes,
) {
  if (type == AttendanceType.clockin) {
    if (clockNotes["clockIn"] == "-") {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  } else {
    if (clockNotes["clockOut"] == "-") {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  }
}

Color onSetColorTitleType(
  AttendanceType type,
  LocationType locationType,
  Map clockNotesDateTime,
) {
  if (type == AttendanceType.clockin) {
    if (clockNotesDateTime["clockInNotes"] == "-" &&
        clockNotesDateTime["clockInDateTime"] == "-" &&
        locationType == LocationType.none) {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  } else {
    if (clockNotesDateTime["clockOutNotes"] == "-" &&
        clockNotesDateTime["clockOutDateTime"] == "-" &&
        locationType == LocationType.none) {
      return AppColors.greyDisabled;
    } else {
      return AppColors.basicDark;
    }
  }
}
