import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/utils/adaptive_size.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';

class Snack {
  Snack._();

  static dynamic show(
    SnackbarType snackbarType,
    String title,
    String message, {
    final Color? backgroundColor,
    final Color? colorText,
    final Duration? duration,
  }) {
    if (Get.isSnackbarOpen) {
      return Get.back();
    } else {
      final Color? background;
      if (snackbarType == SnackbarType.error) {
        background = AppColors.redAlert;
      } else {
        if (snackbarType == SnackbarType.success) {
          background = AppColors.greenDark;
        } else {
          if (snackbarType == SnackbarType.info) {
            background = AppColors.bluePrimary;
          } else {
            background = backgroundColor;
          }
        }
      }
      return Get.snackbar(
        title,
        message,
        snackPosition: SnackPosition.BOTTOM,
        colorText: colorText ?? Colors.white,
        backgroundColor: background,
        snackStyle: SnackStyle.FLOATING,
        duration: duration ?? const Duration(milliseconds: 2000),
        padding: EdgeInsets.symmetric(
          vertical: ResponsiveSize.height(1),
          horizontal: ResponsiveSize.width(4),
        ),
        margin: EdgeInsets.symmetric(
          vertical: ResponsiveSize.height(1),
          horizontal: ResponsiveSize.width(2),
        ),
      );
    }
  }
}
