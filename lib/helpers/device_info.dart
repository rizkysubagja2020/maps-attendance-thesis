import 'package:device_info_plus/device_info_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';

class DeviceInfo {
  // private method
  Future<AndroidDeviceInfo> _getDeviceInfoAndroid() async {
    final DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
    final AndroidDeviceInfo _androidInfo = await _deviceInfo.androidInfo;

    return _androidInfo;
  }

  Future<IosDeviceInfo> _getDeviceInfoIos() async {
    final DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
    final IosDeviceInfo _iosInfo = await _deviceInfo.iosInfo;

    return _iosInfo;
  }

  // get id device info
  Future<String?> getDeviceIdAndroid() async {
    final AndroidDeviceInfo _androidInfo = await _getDeviceInfoAndroid();
    return _androidInfo.androidId;
  }

  Future<String?> getDeviceIdIos() async {
    final IosDeviceInfo _iosInfo = await _getDeviceInfoIos();
    return _iosInfo.identifierForVendor;
  }

  // get sdk version
  Future<int?> getDeviceSdkAndroid() async {
    final AndroidDeviceInfo _androidInfo = await _getDeviceInfoAndroid();
    return _androidInfo.version.sdkInt;
  }

  Future<String?> getVersionApp() async {
    final PackageInfo _appInfo = await PackageInfo.fromPlatform();
    return _appInfo.version;
  }

  Future<String?> getAppName() async {
    final PackageInfo _appInfo = await PackageInfo.fromPlatform();
    return _appInfo.appName;
  }
}
