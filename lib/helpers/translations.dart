import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
import 'package:maps_attendance_thesis/lang/english_us.dart';
import 'package:maps_attendance_thesis/utils/country_code.dart';
import 'package:maps_attendance_thesis/utils/language_code.dart';
import 'package:maps_attendance_thesis/utils/preferences_key.dart';

class MapsAttendanceTranslations extends Translations {
  // default language 'en', 'US'
  static const locale = Locale(LanguageCode.englishUS, CountryCode.unitedState);
  static const fallBackLocale = Locale(
    LanguageCode.englishUS,
    CountryCode.unitedState,
  );

  dynamic initLocale() async {
    final SharedPref _sharedPref = SharedPref();
    // check if language has been write before
    final bool isPreferredLang = await _sharedPref.hasData(
      PreferencesKey.keyLanguage,
    );

    if (!isPreferredLang) {
      await _sharedPref.writeLanguage();
      await _sharedPref.writeCountry();
    } else {
      final String? _getLanguage = await _sharedPref.readLanguage();
      final String? _getCountry = await _sharedPref.readCountry();
      // language 'id', 'ID'
      Get.updateLocale(Locale(_getLanguage!, _getCountry));
    }
  }

  @override
  Map<String, Map<String, String>> get keys => {
        "en_US": englishUS,
      };
}
