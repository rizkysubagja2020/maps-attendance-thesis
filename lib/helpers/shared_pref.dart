import 'package:maps_attendance_thesis/utils/country_code.dart';
import 'package:maps_attendance_thesis/utils/language_code.dart';
import 'package:maps_attendance_thesis/utils/preferences_key.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  // instances
  final Future<SharedPreferences> _prefsInstance =
      SharedPreferences.getInstance();

  // save access token
  Future<void> writeAccessToken(String accesstoken) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyAccessToken, accesstoken);
  }

  // save refresh token
  Future<void> writeRefreshToken(String refreshToken) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyRefreshToken, refreshToken);
  }

  // save translation
  Future<void> writeLanguage([
    String languageCode = LanguageCode.englishUS,
  ]) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyLanguage, languageCode);
  }

  Future<void> writeCountry([
    String countryCode = CountryCode.unitedState,
  ]) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyCountry, countryCode);
  }

  // save user profile
  Future<void> writeFullName(String fullNameParam) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyUserProfileName, fullNameParam);
  }

  // save change password status
  Future<void> writeChangePasswordStatus({
    required bool isChangePassword,
  }) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setBool(PreferencesKey.keyChangePassword, isChangePassword);
  }

  Future<void> writeChangePasswordMessage(String message) async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.setString(PreferencesKey.keyChangePasswordMessage, message);
  }

  // read access token
  Future<String?> readAccessToken() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _accessToken =
        _prefs.getString(PreferencesKey.keyAccessToken);
    return _accessToken;
  }

  // read refresh token
  Future<String?> readRefreshToken() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _refreshToken =
        _prefs.getString(PreferencesKey.keyRefreshToken);
    return _refreshToken;
  }

  // read language for translation, default : English
  Future<String?> readLanguage() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _lang = _prefs.getString(PreferencesKey.keyLanguage);
    return _lang;
  }

  // read country for translation, default : United State
  Future<String?> readCountry() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _country = _prefs.getString(PreferencesKey.keyCountry);
    return _country;
  }

  // read user profile
  Future<String?> readUserProfileName() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _profileName = _prefs.getString(
      PreferencesKey.keyUserProfileName,
    );
    return _profileName;
  }

  // read change password status
  Future<bool?> readChangePasswordStatus() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final bool? _isChangePassword = _prefs.getBool(
      PreferencesKey.keyChangePassword,
    );
    return _isChangePassword;
  }

  Future<String?> readChangePasswordMessage() async {
    final SharedPreferences _prefs = await _prefsInstance;
    final String? _changePasswordMessage = _prefs.getString(
      PreferencesKey.keyChangePasswordMessage,
    );
    return _changePasswordMessage;
  }

  // remove access token
  Future<void> removeAccessToken() async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.remove(PreferencesKey.keyAccessToken);
  }

  // remove refresh token
  Future<void> removeRefreshsToken() async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.remove(PreferencesKey.keyRefreshToken);
  }

  // remove user profile
  Future<void> removeUserProfileName() async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.remove(PreferencesKey.keyUserProfileName);
  }

  // remove change password status
  Future<void> removeChangePasswordStatus() async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.remove(PreferencesKey.keyChangePassword);
  }

  Future<void> removeChangePasswordMessage() async {
    final SharedPreferences _prefs = await _prefsInstance;
    _prefs.remove(PreferencesKey.keyChangePasswordMessage);
  }

  Future<bool> hasData(String key) async {
    final SharedPreferences _prefs = await _prefsInstance;
    return _prefs.containsKey(key);
  }
}
