import 'package:get/get.dart';
import 'package:maps_attendance_thesis/views/attendance/attendance_page.dart';
import 'package:maps_attendance_thesis/views/authorize/authorize_page.dart';
import 'package:maps_attendance_thesis/views/frame_view/frame_view.dart';
import 'package:maps_attendance_thesis/views/login/login_page.dart';

class AppRoutes {
  AppRoutes._();
  static final routes = [
    GetPage(name: "/frame", page: () => FrameView()),
    GetPage(name: "/authorize", page: () => AuthorizeView()),
    GetPage(name: "/home", page: () => AttendancePage()),
    GetPage(name: "/login", page: () => LoginPage()),
  ];
}