import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/translations.dart';
import 'package:maps_attendance_thesis/routes/routes.dart';
import 'package:maps_attendance_thesis/services/initiate.dart';
import 'package:maps_attendance_thesis/views/authorize/authorize_page.dart';
import 'package:responsive_framework/responsive_framework.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  initiate();
  runApp(MAT());
}

class MAT extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        minWidth: 375,
        breakpoints: const [
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
      title: "Maps Attendance Thesis",
      debugShowCheckedModeBanner: false,
      translations: MapsAttendanceTranslations(),
      locale: MapsAttendanceTranslations.locale,
      fallbackLocale: MapsAttendanceTranslations.fallBackLocale,
      getPages: AppRoutes.routes,
      defaultTransition: Transition.noTransition,
      home: AuthorizeView(),
      // home: LoginPage(),
    );
  }
}