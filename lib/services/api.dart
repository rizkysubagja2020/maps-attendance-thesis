import 'package:maps_attendance_thesis/utils/enums.dart';

// API Class for initialize url api, client id and client secret
class API {
  // url api for all request data from server
  // demo api:
  static String host = "https://demo.dreite.co.id/dos/api/mobile/web/v1";

  // live api:
  // static String host = "https://dos.dreite.co.id/api/mobile/web/v1";

  /// client id & client secret: used for header data validation
  static String clientId =
      "rf22vL2M8PYDYGLVLCJqzLKjjWBpDtpzBE5yFSXwmsZEP4XQ5JrEPGjuvjaLANtMjJjyxFSpHeuAzWTY";
  static String clientSecret =
      "wX3nyMXcB8erN5kyHu8aapgDASZmXgYTFHLtSmfbE5pPDSwuxVqqUnMasn4YFEJNXH4mpz6t6MSLxLHd";

  // API endpoint
  String? endPoint(EndPointName endPointName, [String? query]) {
    final Map<EndPointName, String> _urlAPI = {
      // request post authorize, access token and refresh token
      EndPointName.authorize: "/user/authorize",
      EndPointName.accessToken: "/user/access-token",
      EndPointName.refreshToken: "/user/refresh-token",
      // user
      EndPointName.userInfo: "/user/info",
      EndPointName.userLogout: "/user/logout",
      EndPointName.userChangePassword: "/user/change-password",
      // app
      EndPointName.appInfo: "/app/info",
      // attendance
      EndPointName.attendanceInfo: "/attendance/info",
      EndPointName.attendanceCalendar: "/calendar/get-data?$query",
      EndPointName.attendanceHistory: "/attendance/history?$query",
      EndPointName.attendanceType: "/attendance/$query",
    };

    // return endpoint
    return _urlAPI[endPointName];
  }
}
