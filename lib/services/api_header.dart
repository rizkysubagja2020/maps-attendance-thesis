// Header Class for handle header data when request to url api

import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';

import 'api.dart';

class ApiHeaders {
  /// This method will generate Map data for header
  Future<Map<String, String>> result({
    bool hasAccessToken = true,
  }) async {
    /// Initialize box for get storage value
    final SharedPref _sharedPref = SharedPref();

    /// Init deviceInfo and androidInfo for get detail device
    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    /// Call getIp address value
    final ipAddress = await _getLocalIpAddress();

    final Map<String, String> header = {
      "Auth-Client-Id": API.clientId,
      "Auth-Client-Secret": API.clientSecret,
      "Req-Ip": ipAddress.toString(),
    };

    if (Platform.isAndroid) {
      final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (hasAccessToken) {
        final String? accessToken = await _sharedPref.readAccessToken();
        header.addAll({
          "Auth-Access-Token": accessToken ?? "",
          "Auth-Device-Id": androidInfo.androidId!,
        });
      }
    } else {
      final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      if (hasAccessToken) {
        final String? accessToken = await _sharedPref.readAccessToken();
        header.addAll({
          "Auth-Access-Token": accessToken ?? "",
          "Auth-Device-Id": iosInfo.identifierForVendor!,
        });
      }
    }

    return header;
  }

  /// This method for get ip address from device
  /// Will return ip address value as string
  Future<String?> _getLocalIpAddress() async {
    final interfaces = await NetworkInterface.list(
      type: InternetAddressType.IPv4,
      includeLinkLocal: true,
    );

    try {
      // Try VPN connection first
      final NetworkInterface vpnInterface =
          interfaces.firstWhere((element) => element.name == "tun0");
      return vpnInterface.addresses.first.address;
      // ignore: avoid_catching_errors
    } on StateError {
      // Try wlan connection next
      try {
        final NetworkInterface interface =
            interfaces.firstWhere((element) => element.name == "wlan0");
        return interface.addresses.first.address;
      } catch (ex) {
        // Try any other connection next
        try {
          final NetworkInterface interface = interfaces.firstWhere(
            (element) => !(element.name == "tun0" || element.name == "wlan0"),
          );
          return interface.addresses.first.address;
        } catch (ex) {
          return null;
        }
      }
    }
  }
}
