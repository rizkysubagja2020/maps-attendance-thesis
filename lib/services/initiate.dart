import 'dart:io';
import 'package:in_app_update/in_app_update.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:maps_attendance_thesis/helpers/translations.dart';

dynamic initiate() async {
  final MapsAttendanceTranslations _appTranslations = MapsAttendanceTranslations();
  // date formating
  await initializeDateFormatting();
  // initiate localization
  _appTranslations.initLocale();


  // // App update
  // if (Platform.isAndroid) {
  //   try {
  //     InAppUpdate.startFlexibleUpdate().then(
  //       (value) => {
  //         InAppUpdate.completeFlexibleUpdate(),
  //         Snack.show(
  //           SnackbarType.success,
  //           "Success",
  //           "Update Successfully",
  //         ),
  //       },
  //     );
  //   } catch (e) {
  //     return Snack.show(
  //       SnackbarType.error,
  //       "Failed",
  //       "Update error : $e",
  //     );
  //   }
  // }
}
