import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/status_log.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'api.dart';
import 'api_header.dart';

class ApiRequest {
  // initiate
  static String _path = "";

  final API _api = API();
  static final Dio _dio = Dio(
    BaseOptions(
      connectTimeout: 30000,
      baseUrl: API.host,
      contentType: ContentType.json.toString(),
    ),
  )..interceptors.addAll([
      // RetryOnConnectionChangeInterceptor(
      //   requestRetrier: RequestRetrier(
      //     dio: Dio(),
      //     connectivity: Connectivity(),
      //   ),
      // ),
      InterceptorsWrapper(
        onRequest: (options, handler) {
          _path = options.path;
          log("ON INTERCEPTOR REQUEST $_path", name: StatusLog.logIntercept);
          return handler.next(options); //continue
        },
        onResponse: (response, handler) async {
          log("ON INTERCEPTOR RESPONSE", name: StatusLog.logIntercept);
          log("ON INTERCEPTOR $response", name: StatusLog.logIntercept);

          return handler.next(response); // continue
        },
        onError: (DioError e, handler) async {
          log("ON INTERCEPTOR ERROR", name: StatusLog.logIntercept);
          log("ON INTERCEPTOR ${e.response}", name: StatusLog.logIntercept);
          log(
            "ON INTERCEPTOR ${e.response.runtimeType}",
            name: StatusLog.logIntercept,
          );
        },
      ),
      PrettyDioLogger(),
    ]);

  Future<Map<String, dynamic>> postFileRepository({
    required EndPointName endPoint,
    required Map<String, dynamic> body,
    required String type,
    File? imageFile,
    bool hasAccessToken = false,
  }) async {
    // header
    final Map<String, dynamic> _header = await ApiHeaders().result(
      hasAccessToken: hasAccessToken,
    );
    log("HEADER = $_header", name: StatusLog.logHeader);
    // endpoint api
    final String? _endpoint = _api.endPoint(endPoint, type);
    log("ENDPOINT = $_endpoint", name: StatusLog.logEndPoint);

    final dynamic _bodyMultiPart;

    if (imageFile != null) {
      final Map<String, dynamic> _containMap = {
        "image_file": await MultipartFile.fromFile(
          imageFile.path,
          filename: imageFile.path.split('/').last,
          contentType: MediaType("image", "jpg"),
        ),
      };
      body.forEach((key, value) {
        _containMap[key] = value;
      });
      _bodyMultiPart = FormData.fromMap(_containMap);
      log(_containMap.toString(), name: StatusLog.logParameter);
    } else {
      _bodyMultiPart = FormData.fromMap(body);
      log(body.toString(), name: StatusLog.logParameter);
    }

    final Response _response = await _dio.post(
      _endpoint.toString(),
      data: _bodyMultiPart,
      options: Options(
        headers: _header,
        receiveTimeout: 30 * 1000,
      ),
    );
    log("response success : ${_response.data}", name: StatusLog.logResponse);
    return _response.data as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> postRepository({
    required EndPointName endPoint,
    required Map body,
    bool hasAccessToken = false,
  }) async {
    // header
    final Map<String, dynamic> _header = await ApiHeaders().result(
      hasAccessToken: hasAccessToken,
    );
    log("HEADER = $_header", name: StatusLog.logHeader);
    // endpoint api
    final String? _endpoint = _api.endPoint(endPoint);
    log("ENDPOINT = $_endpoint", name: StatusLog.logEndPoint);

    try {
      final Response _response = await _dio.post(
        _endpoint.toString(),
        data: body,
        options: Options(
          headers: _header,
          receiveTimeout: 30 * 1000,
        ),
      );
      log("response success : ${_response.data}", name: StatusLog.logResponse);
      return _response.data as Map<String, dynamic>;
    } on DioError catch (err) {
      // error handling in here
      final Map<String, dynamic> error = {
        "code": err.response?.statusCode,
        "message": err.response?.statusMessage,
      };

      return error;
    }
  }

  Future<Map<String, dynamic>> getRepository({
    required EndPointName endPoint,
    bool hasAccessToken = false,
    String? query = "",
  }) async {
    // header
    final Map<String, dynamic> _header =
        await ApiHeaders().result(hasAccessToken: hasAccessToken);
    log("HEADER = $_header", name: StatusLog.logHeader);
    // endpoint api
    final String? _endpoint = _api.endPoint(endPoint, query);
    log("ENDPOINT = $_endpoint", name: StatusLog.logEndPoint);

    final Response _response = await _dio.get(
      _endpoint.toString(),
      options: Options(headers: _header),
    );
    log(
      "response success $endPoint : ${_response.data}",
      name: StatusLog.logResponse,
    );
    return _response.data as Map<String, dynamic>;
  }
}
