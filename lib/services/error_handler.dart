import 'dart:developer';

import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
import 'package:maps_attendance_thesis/helpers/snackbar.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/status_code.dart';

import 'api_request.dart';

class ErrorHandler {
  static final SharedPref _sharedPref = SharedPref();

  Future<bool> refreshToken() async {
    final ApiRequest _apiRequest = ApiRequest();
    final _accessToken = await _sharedPref.readAccessToken();
    final _refreshToken = await _sharedPref.readRefreshToken();

    final Map body = {
      "access_token": _accessToken,
      "refresh_token": _refreshToken,
    };

    final Map<String, dynamic> _result = await _apiRequest.postRepository(
      endPoint: EndPointName.refreshToken,
      body: body,
    );

    debugPrint(_result.toString(), wrapWidth: 1000);

    if (_result["code"] == StatusCode.codeOk) {
      // save access token and refresh token
      final String _accessToken =
          _result["data"]["token"]["access_token"] as String;
      final String _refreshToken =
          _result["data"]["token"]["refresh_token"] as String;
      await _sharedPref.writeAccessToken(_accessToken);
      await _sharedPref.writeRefreshToken(_refreshToken);

      return true;
    } else {
      return false;
    }
  }

  dynamic userLogout() async {
    _sharedPref.removeAccessToken();
    _sharedPref.removeRefreshsToken();

    GetInstance().resetInstance();

    Get.offAllNamed("/authorize");
  }

  // other handling errors
  Future<bool> handler(Map<String, dynamic> result) async {
    // call error handler method
    final Map<String, dynamic> _messageAndErrorCode = _messageErrorCodeHandler(
      result,
    );
    // return action by error code checker method
    final int _errorCode = _messageAndErrorCode["errorCode"] as int;
    final String _errorMessage = _messageAndErrorCode["errorMessage"] as String;
    log("_errorCode = $_errorCode");
    log("_errorMessage = $_errorMessage");
    final bool _resultStatus = await _codeChecker(_errorCode, _errorMessage);
    return _resultStatus;
  }

  Map<String, dynamic> _messageErrorCodeHandler(Map result) {
    // final _code = result["code"];
    final bool _isContainErrorKey = result.containsKey("error");
    late int _errorCode;
    late String _errorMessage;

    // check is contain error key
    if (_isContainErrorKey) {
      final bool _isContainMessageKey =
          result["error"].containsKey("message") as bool;
      final bool _isContainCodeKey =
          result["error"].containsKey("code") as bool;

      final String _messageKey = result["error"].keys.elementAt(0) as String;

      if (_isContainMessageKey) {
        if (result["error"]["message"] is String) {
          _errorMessage = result["error"]["message"] as String;
        } else {
          final String meessageErrorKey = _isContainErrorKey
              ? result["error"]["message"].keys.elementAt(0).toString()
              : "";
          _errorMessage =
              result["error"]["message"][meessageErrorKey][0] as String;
        }
      } else {
        _errorMessage = result["error"][_messageKey][0] as String;
      }

      _errorCode = _isContainCodeKey
          ? result["error"]["code"] as int
          : result["code"] as int;
    } else {
      _errorCode = result["code"] as int;
      _errorMessage = result["message"] as String;
    }

    return {"errorCode": _errorCode, "errorMessage": _errorMessage};
  }

  Future<bool> _codeChecker(int code, String errorMessage) async {
    switch (code) {
      case StatusCode.codeUnauthorized:
        Snack.show(
          SnackbarType.error,
          "info_label".tr,
          errorMessage,
        );
        return false;
      case StatusCode.codeBadRequest:
      case StatusCode.appCodeUserInvalid:
      case StatusCode.appCodeValidateData:
      case StatusCode.appCodeEmptyBody:
      case StatusCode.appCodeTokenInvalid:
      case StatusCode.appCodeAuthInvalid:
        log("REFRESH TOKEN IN HERE");
        final bool _isRefreshSuccess = await refreshToken();
        if (_isRefreshSuccess) {
          return true;
        } else {
          userLogout();
          return false;
        }
      default:
        log("DEFAULT HANDLE ERROR $errorMessage $code");
        return false;
    }
  }
}
