import 'dart:async';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AttendanceController extends GetxController {

  Timer? _timer;

  String dateTimeNow = DateTime.now().toString();
  
  @override
  void onInit() {
    _startCountdown();
    super.onInit();
  }

  void _startCountdown() {
    const sec = Duration(seconds: 1);

    _timer = Timer.periodic(sec, (timer) {
      final DateTime incrementTime = DateTime.parse(dateTimeNow);
      final updatedTime = incrementTime.add(const Duration(seconds: 1));

      dateTimeNow = updatedTime.toString();

      final formattedMinuteSecond = int.parse(
        DateFormat("mmss").format(
          DateTime.parse(dateTimeNow),
        ),
      );

      if (formattedMinuteSecond == 0000) {
        log("0000");
      }
      update(["attendanceInfo"]);
    });
  }

}