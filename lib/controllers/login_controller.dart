import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/device_info.dart';
import 'package:maps_attendance_thesis/helpers/location.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
import 'package:maps_attendance_thesis/helpers/snackbar.dart';
import 'package:maps_attendance_thesis/helpers/string_modifiers.dart';
import 'package:maps_attendance_thesis/repository/tokenRepository/token_repository.dart';
import 'package:maps_attendance_thesis/repository/tokenRepository/token_repository_interface.dart';
import 'package:maps_attendance_thesis/repository/userInfoRepository/user_info_repository.dart';
import 'package:maps_attendance_thesis/repository/userInfoRepository/user_info_repository_interface.dart';
import 'package:maps_attendance_thesis/services/error_handler.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/status_code.dart';
import 'package:maps_attendance_thesis/views/widgets/dialog/forgot_password_dialog.dart';

class LoginController extends GetxController {
  // helper instance
  final IUserInfoRepository _userInfoRepository = UserInfoRepository();
  final ITokenRepository _tokenRepository = TokenRepository();
  final SharedPref _sharedPref = SharedPref();
  final Location _location = Location();
  final ErrorHandler _errorHandler = ErrorHandler();

  // widget controller
  TextEditingController nikController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  ScrollController scrollController = ScrollController();

  // focus node
  FocusNode nikFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  // change to "DataType?" varName when migration to null safety
  double latitude = 0.0;
  double longitude = 0.0;
  String authorizationCode = "";
  List resultErrorPassword = [];
  String resultErrorMessage = "";
  String resultErrorDenied = "";
  String resultUserPassInvalid = "";
  Rx<String?> versionApp = Rx<String>("");
  Rx<String?> appName = Rx<String>("");
  RxString company = "".obs;
  RxString copyright = "".obs;
  RxBool isLoading = false.obs;
  RxInt year = DateTime.now().year.obs;
  RxBool isHideVersion = false.obs;

  // validation
  bool validateAccessUser = false;
  bool validateForgotPassword = false;
  bool validateUserPassInvalid = false;

  @override
  void onInit() {
    // AnalyticsHelper.analyticCurrentScreen(AnalyticsType.loginScreen);
    super.onInit();
    _validateChangePasswordFeedback();
    _getCompanyInfo();
    _getApplicatinInfo();
    _getCurrentLocation();
    _onKeyboardListener();
  }

  dynamic _validateChangePasswordFeedback() async {
    final bool? _isChangePassword =
        await _sharedPref.readChangePasswordStatus();
    final String? _changePasswordMessage =
        await _sharedPref.readChangePasswordMessage();

    if (_isChangePassword != null && _changePasswordMessage != null) {
      if (_isChangePassword) {
        Snack.show(
          SnackbarType.info,
          "label_info".tr,
          _changePasswordMessage,
        );
        _sharedPref.removeChangePasswordStatus();
        _sharedPref.removeChangePasswordMessage();
      }
    }
  }

  Future<void> _getApplicatinInfo() async {
    // package info
    final String? _packageInfoVersion = await DeviceInfo().getVersionApp();
    final String? _packageInfoName = await DeviceInfo().getAppName();
    versionApp.value = _packageInfoVersion;
    appName.value = _packageInfoName;
  }

  void _onKeyboardListener() {
    nikFocusNode.addListener(() {
      final bool hasFocus = nikFocusNode.hasFocus;
      hasFocus ? isHideVersion.value = true : isHideVersion.value = false;
    });

    passwordFocusNode.addListener(() {
      final bool hasFocus = passwordFocusNode.hasFocus;
      hasFocus ? isHideVersion.value = true : isHideVersion.value = false;
    });
  }

  dynamic _getCurrentLocation() async {
    final List<double> _getCoordinate = await _location.getCurrentLocation();
    longitude = _getCoordinate[0];
    latitude = _getCoordinate[1];
  }

  Future<bool> _getCompanyInfo() async {
    final Map<String, dynamic> _result = await _userInfoRepository.getAppInfo();
    if (_result["code"] == StatusCode.codeOk) {
      company.value = _result["data"]["company"] as String;
      copyright.value = _result["data"]["copyright"] as String;
      return true;
    } else {
      return false;
    }
  }

  Future<bool> _getValidateUser() async {
    final Map<String, dynamic> _result =
        await _userInfoRepository.getUserInfo();
    if (_result["code"] == StatusCode.codeOk) {
      ellipsisName(_result["data"]["name"].toString());
      isLoading.value = false;
      return true;
    } else if (_result["code"] == StatusCode.codeUnauthorized) {
      resultErrorDenied = _result["error"]["message"] as String;
      log("error denied = $resultErrorDenied");
      isLoading.value = false;
      return false;
    } else {
      final bool _isRecall = await _errorHandler.handler(_result);
      if (_isRecall) {
        final bool _isSuccess = await _getValidateUser();
        if (_isSuccess) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  Future<bool> _postUserAuthorized() async {
    isLoading.value = true;
    final Map<String, dynamic> _result = await _tokenRepository.postAuthorize(
      nikController.text,
      passwordController.text,
      longitude,
      latitude,
    );
    if (_result["code"] == StatusCode.codeOk) {
      authorizationCode = _result["data"]["authorization"]["code"] as String;
      return true;
    } else if (_result["code"] == StatusCode.appCodeValidateData) {
      resultErrorMessage = _result["message"].toString();
      resultErrorPassword =
          _result["error"]["message"]["password"] as List<dynamic>;
      isLoading.value = false;
      return false;
    } else {
      isLoading.value = false;
      return false;
    }
  }

  Future<bool> _postUserAccessToken() async {
    final Map<String, dynamic> _result = await _tokenRepository.postAccessToken(
      authorizationCode,
    );
    // isLoading.value = false;
    debugPrint(_result.toString(), wrapWidth: 1000);

    if (_result["code"] == StatusCode.codeOk) {
      _sharedPref.writeAccessToken(
        _result["data"]["token"]["access_token"] as String,
      );
      _sharedPref.writeRefreshToken(
        _result["data"]["token"]["refresh_token"] as String,
      );
      return true;
    } else {
      return false;
    }
  }

  // validation login form on button login
  dynamic validateLoginForm() {
    // set default
    resultErrorPassword.clear();
    validateUserPassInvalid = false;
    validateAccessUser = false;
    validateForgotPassword = false;
    update(["validateUserPassword"]);

    if (nikController.text.length < 4) {
      resultUserPassInvalid = "general_not_valid_msg".trParams(
        {"type": "login_form_nik".tr},
      );
      validateUserPassInvalid = true;
      update(["validateUserPassword"]);
      Snack.show(
        SnackbarType.error,
        "login_form_nik".tr,
        resultUserPassInvalid,
      );
      // Future.delayed(const Duration(seconds: 5), () {
      //   validateUserPassInvalid = false;
      //   update(["validateUserPassword"]);
      // });
    } else if (passwordController.text.length < 6) {
      resultUserPassInvalid = "general_not_valid_msg".trParams(
        {"type": "login_form_password".tr},
      );
      validateUserPassInvalid = true;
      update(["validateUserPassword"]);
      Snack.show(
        SnackbarType.error,
        "login_form_password".tr,
        "general_not_valid_msg".trParams(
          {"type": "login_form_password".tr},
        ),
      );
      // Future.delayed(const Duration(seconds: 5), () {
      //   validateUserPassInvalid = false;
      //   update(["validateUserPassword"]);
      // });
    } else {
      _onLogin();
    }
  }

  dynamic _onLogin() async {
    // dismiss keyboard
    FocusManager.instance.primaryFocus?.unfocus();

    final bool _isTokenActive = await _postUserAuthorized();

    if (_isTokenActive) {
      final bool _isLoginSuccess = await _postUserAccessToken();
      if (_isLoginSuccess) {
        final bool _isUserValidated = await _getValidateUser();
        if (_isUserValidated) {
          GetInstance().resetInstance();
          Get.offAllNamed("/frame");
          // send data analytic
          // AnalyticsHelper.analyticLogin();
        } else {
          validateAccessUser = true;
          validateForgotPassword = false;
          validateUserPassInvalid = false;
          update(["validateUserPassword"]);
          // Future.delayed(const Duration(seconds: 5), () {
          //   validateAccessUser = false;
          //   update(["validateUserPassword"]);
          // });
          Snack.show(
            SnackbarType.error,
            "Invalid Access User",
            "Please contact administrator",
          );
        }
      }
    } else {
      validateAccessUser = true;
      validateForgotPassword = false;
      validateUserPassInvalid = false;
      update(["validateUserPassword"]);
      // Future.delayed(const Duration(seconds: 5), () {
      //   validateAccessUser = false;
      //   update(["validateUserPassword"]);
      // });
      Snack.show(
        SnackbarType.error,
        "${resultErrorPassword[0]}",
        resultErrorMessage,
      );
    }
  }

  void onPressForgotPassword(BuildContext context) {
    validateUserPassInvalid = false;
    validateAccessUser = false;
    validateForgotPassword = true;
    update(["validateUserPassword"]);
    // show dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return ForgotPasswordDialog();
        // return ClockSuccessDialog(locationType: 0);
      },
    );
  }

  // function to predict which button is clicked
  dynamic validateUserAction() {
    return validateLoginForm();
  }

  // scroll controller
  dynamic resetScrollView() {
    scrollController.animateTo(
      0.0,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
