import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/device_info.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
import 'package:maps_attendance_thesis/helpers/string_modifiers.dart';
import 'package:maps_attendance_thesis/repository/authorizeRepository/authorize_repository.dart';
import 'package:maps_attendance_thesis/repository/authorizeRepository/authorize_repository_interface.dart';
import 'package:maps_attendance_thesis/services/error_handler.dart';
import 'package:maps_attendance_thesis/utils/preferences_key.dart';
import 'package:maps_attendance_thesis/utils/status_code.dart';

class AuthorizeController extends GetxController {
  // helper instance
  final IAuthorizeRepository _authorizeRepository = AuthorizeRepository();
  final SharedPref _sharedPref = SharedPref();
  final ErrorHandler _errorHandler = ErrorHandler();

  Rx<String?> versionApp = Rx<String>("");
  Rx<String?> appName = Rx<String>("");
  RxInt year = DateTime.now().year.obs;

  @override
  void onInit() {
    super.onInit();
    _getApplicatinInfo();
    _validateSession();
  }

  dynamic _validateSession() async {
    final bool _hasToken = await _sharedPref.hasData(
      PreferencesKey.keyAccessToken,
    );

    Future.delayed(const Duration(milliseconds: 200), () async {
      if (_hasToken) {
        final bool _isValidate = await _getValidateUser();
        if (_isValidate) {
          Get.offNamedUntil("/frame", (route) => false);
        } else {
          Get.offAllNamed("/login");
        }
      } else {
        Get.offAllNamed("/login");
      }
    });
  }

  Future<bool> _getValidateUser() async {
    final Map<String, dynamic> _result =
        await _authorizeRepository.getUserInfo();
    if (_result["code"] == StatusCode.codeOk) {
      ellipsisName(_result["data"]["name"].toString());
      return true;
    } else {
      final bool _isRecall = await _errorHandler.handler(_result);
      if (_isRecall) {
        final bool _isSuccess = await _getValidateUser();
        if (_isSuccess) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  Future<void> _getApplicatinInfo() async {
    // package info
    final String? _packageInfoVersion = await DeviceInfo().getVersionApp();
    final String? _packageInfoName = await DeviceInfo().getAppName();
    versionApp.value = _packageInfoVersion;
    appName.value = _packageInfoName;
  }
}
