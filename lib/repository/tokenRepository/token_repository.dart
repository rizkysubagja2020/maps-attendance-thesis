import 'dart:developer';
import 'dart:io';

import 'package:maps_attendance_thesis/helpers/device_info.dart';
import 'package:maps_attendance_thesis/services/api_request.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';

import 'token_repository_interface.dart';

class TokenRepository implements ITokenRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> postAuthorize(
    String username,
    String password,
    double longitude,
    double latitude,
  ) async {
    // package info
    final String? _packageInfoVersion = await DeviceInfo().getVersionApp();

    Map<String, dynamic> body = {};
    if (Platform.isAndroid) {
      final String? _androidInfoId = await DeviceInfo().getDeviceIdAndroid();
      body = {
        "username": username,
        "password": password,
        "device_id": _androidInfoId,
        "device_os": Platform.operatingSystem,
        "device_version_app": _packageInfoVersion,
        "geo_long": longitude.toString(),
        "geo_lat": latitude.toString(),
      };
    } else {
      final String? _iosInfoId = await DeviceInfo().getDeviceIdIos();
      body = {
        "username": username,
        "password": password,
        "device_id": _iosInfoId,
        "device_os": Platform.operatingSystem,
        "device_version_app": _packageInfoVersion,
        "geo_long": longitude.toString(),
        "geo_lat": latitude.toString(),
      };
    }

    log("BODY POST = $body");

    final Map<String, dynamic> _result = await _apiRequest.postRepository(
      endPoint: EndPointName.authorize,
      body: body,
    );

    return _result;
  }

  @override
  Future<Map<String, dynamic>> postAccessToken(String authCode) async {
    final Map body = {"authorization_code": authCode};

    log("BODY POST = $body");

    final Map<String, dynamic> _result = await _apiRequest.postRepository(
      endPoint: EndPointName.accessToken,
      body: body,
    );

    return _result;
  }
}
