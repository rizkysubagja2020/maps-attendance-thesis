abstract class ITokenRepository {
  // post authorize for get authorize code
  Future<Map<String, dynamic>> postAuthorize(
    String username,
    String password,
    double longitude,
    double latitude,
  );
  // after post authorize then gain access token
  Future<Map<String, dynamic>> postAccessToken(String authCode);
}
