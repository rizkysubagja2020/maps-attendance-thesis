abstract class IAuthorizeRepository {
  Future<Map<String, dynamic>> getUserInfo();
}
