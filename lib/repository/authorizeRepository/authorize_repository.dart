import 'package:maps_attendance_thesis/repository/authorizeRepository/authorize_repository_interface.dart';
import 'package:maps_attendance_thesis/services/api_request.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';

class AuthorizeRepository implements IAuthorizeRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getUserInfo() async {
    final Map<String, dynamic> _result = await _apiRequest.getRepository(
      endPoint: EndPointName.userInfo,
      hasAccessToken: true,
    );

    return _result;
  }
}
