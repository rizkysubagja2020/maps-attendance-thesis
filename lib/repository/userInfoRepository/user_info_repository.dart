import 'package:maps_attendance_thesis/repository/userInfoRepository/user_info_repository_interface.dart';
import 'package:maps_attendance_thesis/services/api_request.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';

class UserInfoRepository implements IUserInfoRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getAppInfo() async {
    final Map<String, dynamic> _result = await _apiRequest.getRepository(
      endPoint: EndPointName.appInfo,
    );

    return _result;
  }

  @override
  Future<Map<String, dynamic>> getUserInfo() async {
    final Map<String, dynamic> _result = await _apiRequest.getRepository(
      endPoint: EndPointName.userInfo,
      hasAccessToken: true,
    );

    return _result;
  }

  @override
  Future<Map<String, dynamic>> postUserLogout(String? accessToken) async {
    final Map body = {
      "access_token": accessToken ?? "",
    };
    final Map<String, dynamic> _result = await _apiRequest.postRepository(
      endPoint: EndPointName.userLogout,
      body: body,
      hasAccessToken: true,
    );

    return _result;
  }

  @override
  Future<Map<String, dynamic>> postChangePassword(
    String oldPassword,
    String newPassword,
    String reTypePassword,
  ) async {
    final Map body = {
      "old_password": oldPassword,
      "new_password": newPassword,
      "new_password_confirmation": reTypePassword,
    };

    final Map<String, dynamic> _result = await _apiRequest.postRepository(
      endPoint: EndPointName.userChangePassword,
      body: body,
      hasAccessToken: true,
    );

    return _result;
  }
}
