abstract class IUserInfoRepository {
  Future<Map<String, dynamic>> getAppInfo();
  Future<Map<String, dynamic>> getUserInfo();
  Future<Map<String, dynamic>> postUserLogout(String? accessToken);
  Future<Map<String, dynamic>> postChangePassword(
    String oldPassword,
    String newPassword,
    String reTypePassword,
  );
}
