import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/controllers/authorize_controller.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/frame/frame_scaffold.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AuthorizeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AuthorizeViewState();
}

class AuthorizeViewState extends State<AuthorizeView> {
  final AuthorizeController _authorizeController = Get.put(
    AuthorizeController(),
  );

  late Image _imageLogo;

  @override
  void initState() {
    super.initState();
    _imageLogo = Image.asset(Assets.imageSplashScreen);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_imageLogo.image, context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.whiteBackground,
        systemNavigationBarIconBrightness: Brightness.dark,
      ),
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        color: Platform.isIOS ? AppColors.whiteBackground : null,
        statusBarColor: AppColors.whiteBackground,
        statusBarIconBrightness: Brightness.dark,
        view: Container(
          color: AppColors.whiteBackground,
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Expanded(
                  child: Align(child: Image.asset(Assets.imageSplashScreen)),
                ),
              ),
              ResponsiveRowColumnItem(
                child: SizedBox(
                  height: SizeConfig.blockSizeVertical * 12,
                  child: Column(
                    children: <Widget>[_versionNumber(), _applicationName()],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _versionNumber() {
    return Obx(
      () => RobotoTextView(value: "Version ${_authorizeController.versionApp}"),
    );
  }

  Widget _applicationName() {
    return Obx(
      () => RobotoTextView(
        value:
            "©${_authorizeController.year.value}. ${_authorizeController.appName.value}",
        alignText: AlignTextType.center,
      ),
    );
  }
}
