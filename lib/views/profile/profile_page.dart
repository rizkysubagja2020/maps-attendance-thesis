import 'package:flutter/material.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:responsive_framework/responsive_row_column.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.screenHeight,
      width: SizeConfig.screenWidth,
      color: AppColors.whiteBackground,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: SizeConfig.horizontal(4),
              left: SizeConfig.horizontal(4),
              right: SizeConfig.horizontal(4),
            ),
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: Container(
                  child: const Text(
                    "Coba container"
                  ),
                ),),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _imageProfile() {
    return Container(
      width: SizeConfig.blockSizeHorizontal * 45,
      height: SizeConfig.blockSizeHorizontal * 45,
      decoration: BoxDecoration(
        color: AppColors.whiteBackground,
        shape: BoxShape.circle,
      ),
      child: Image.asset(
        Assets.imageUser,
        width: SizeConfig.blockSizeHorizontal * 45,
        height: SizeConfig.blockSizeHorizontal * 45,
        fit: BoxFit.cover,
      ),
    );
  }
}
