import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;
import 'package:maps_attendance_thesis/controllers/attendance_controller.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/attendance/widgets/attendance_button.dart';
import 'package:maps_attendance_thesis/views/attendance/widgets/attendance_date_time_display.dart';
import 'package:maps_attendance_thesis/views/attendance/widgets/attendance_range_time_display.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AttendancePage extends StatelessWidget {
  final AttendanceController _controller = getx.Get.put(
    AttendanceController(),
    // permanent: true,
  );

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SingleChildScrollView(
      child: Container(
        color: AppColors.whiteBackground,
        child: Stack(
          children: [
            _primaryCurve(context),
            _attendanceContent(),
          ],
        ),
      ),
    );
  }

  Widget _primaryCurve(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 9),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: [
              ResponsiveRowColumnItem(child: _greenMiddleCurve(context))
            ],
          ),
        ),
        _greenDarkCurve(context),
      ],
    );
  }

  Widget _greenMiddleCurve(BuildContext context) {
    return ClipPath(
      clipper: ProsteBezierCurve(
        position: ClipPosition.bottom,
        reclip: false,
        list: [
          BezierCurveSection(
            // start: const Offset(0, 150),
            start: Offset(0, SizeConfig.safeBlockVertical * 40),
            // top: Offset(MediaQuery.of(context).size.width / 4, 100),
            top: Offset(
              MediaQuery.of(context).size.width / 4,
              SizeConfig.safeBlockVertical * 20,
            ),
            // end: Offset(MediaQuery.of(context).size.width / 2, 125),
            end: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 18,
            ),
          ),
          BezierCurveSection(
            // start: Offset(MediaQuery.of(context).size.width / 2, 125),
            start: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 35,
            ),
            // top: Offset(MediaQuery.of(context).size.width / 4 * 3, 150),
            top: Offset(
              MediaQuery.of(context).size.width / 4 * 3,
              SizeConfig.safeBlockVertical * 25,
            ),
            // end: Offset(MediaQuery.of(context).size.width, 125),
            end: Offset(
              MediaQuery.of(context).size.width,
              SizeConfig.safeBlockVertical * 15,
            ),
          ),
        ],
      ),
      child: Container(
        height: SizeConfig.blockSizeVertical * 22,
        color: AppColors.greenMiddle,
      ),
    );
  }

  Widget _greenDarkCurve(BuildContext context) {
    return ClipPath(
      clipper: ProsteBezierCurve(
        position: ClipPosition.bottom,
        reclip: false,
        list: [
          BezierCurveSection(
            // start: const Offset(0, 125),
            start: Offset(0, SizeConfig.safeBlockVertical * 10),
            // top: Offset(MediaQuery.of(context).size.width / 4, 150),
            top: Offset(
              MediaQuery.of(context).size.width / 4,
              SizeConfig.safeBlockVertical * 20,
            ),
            // end: Offset(MediaQuery.of(context).size.width / 2, 125),
            end: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 20,
            ),
          ),
          BezierCurveSection(
            // start: Offset(MediaQuery.of(context).size.width / 2, 125),
            start: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 25,
            ),
            // top: Offset(MediaQuery.of(context).size.width / 4 * 3, 100),
            top: Offset(
              MediaQuery.of(context).size.width / 4 * 3,
              SizeConfig.safeBlockVertical * 18,
            ),
            // end: Offset(MediaQuery.of(context).size.width, 150),
            end: Offset(
              MediaQuery.of(context).size.width,
              SizeConfig.safeBlockVertical * 20,
            ),
          ),
        ],
      ),
      child: Container(
        height: SizeConfig.blockSizeVertical * 22,
        color: AppColors.greenDark,
      ),
    );
  }

  Widget _attendanceContent() {
    return getx.GetBuilder(
      init: _controller,
      id: "attendanceInfo",
      builder: (_) {
        return Container(
          margin: EdgeInsets.only(
            left: SizeConfig.blockSizeHorizontal * 10,
            right: SizeConfig.blockSizeHorizontal * 10,
            top: SizeConfig.blockSizeVertical * 1.5,
          ),
          child: Center(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                    child: AttendanceCurrentTimeDisplay(
                  dateTime: _controller.dateTimeNow,
                )),
                ResponsiveRowColumnItem(
                  child: SizedBox(height: SizeConfig.blockSizeVertical * 5),
                ),
                ResponsiveRowColumnItem(
                  child: AttendanceButton(
                    // onPress: _controller.onShowAttendanceType,
                    // buttonClockType: _controller.getButtonType(),
                    onPress: () => log("Clock In / Clock Out"),
                    buttonClockType: ButtonClockType.buttonClockOut,
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: SizedBox(height: SizeConfig.blockSizeVertical * 2),
                ),
                ResponsiveRowColumnItem(
                  child: Container(
                    height: SizeConfig.screenHeight,
                  ),
                ),
                // const ResponsiveRowColumnItem(
                //     child: AttendanceTimeRangeDisplay(
                //       // infoClockIn: _controller.infoClockIn(),
                //       // infoClockOut: _controller.infoClockOut(),
                //       // timeAdvice: _controller.infoTimeAdvice(),
                //       // timeReminder: _controller.infoReminder(),
                //       // timeMessage: _controller.infoMessage(),
                //       // todayAttendanceInfo:
                //       //     _controller.attendanceInfo["today_history"],
                //       infoClockIn: "-",
                //       infoClockOut: "-",
                //       timeAdvice: "",
                //       timeReminder: "",
                //       timeMessage: "Weekend Saturday",
                //     ),
                //   ),
              ],
            ),
          ),
        );
      },
    );
  }
}
