// Display attendance button clock in or clock out
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AttendanceButton extends StatelessWidget {
  // constructor
  const AttendanceButton({
    required this.onPress,
    required this.buttonClockType,
  });

  final Function() onPress;
  final ButtonClockType buttonClockType;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return _buttonWrapper(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Align(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              columnMainAxisAlignment: MainAxisAlignment.center,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _buttonIconDisplay()),
                ResponsiveRowColumnItem(
                  child: RobotoTextView(
                    value: _buttonTextDisplay(),
                    color: AppColors.whiteBackground,
                    size: SizeConfig.safeBlockHorizontal * 5,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          // Align(child: _buttonIconDisplay()),
        ],
      ),
    );
  }

  Widget _buttonWrapper({required Widget child}) {
    if (buttonClockType == ButtonClockType.buttonClockin ||
        buttonClockType == ButtonClockType.buttonClockOut) {
      return ElevatedButton(
        onPressed: onPress,
        style: ElevatedButton.styleFrom(
          elevation: 10.5,
          primary: _buttonColorDisplay(),
          shape: const CircleBorder(),
          padding: const EdgeInsets.all(24),
          fixedSize: Size(
            SizeConfig.blockSizeHorizontal * 55,
            SizeConfig.blockSizeHorizontal * 55,
          ),
        ),
        child: child,
      );
    } else {
      return Container(
        width: SizeConfig.blockSizeHorizontal * 55,
        height: SizeConfig.blockSizeHorizontal * 55,
        padding: const EdgeInsets.all(24),
        decoration: BoxDecoration(
          color: AppColors.greyDisabled,
          shape: BoxShape.circle,
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 5.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: child,
      );
    }
  }

  Color _buttonColorDisplay() {
    if (buttonClockType == ButtonClockType.buttonClockin ||
        buttonClockType == ButtonClockType.buttonClockOut) {
      return AppColors.bluePrimary;
    } else if (buttonClockType == ButtonClockType.buttonDayOff) {
      return AppColors.greyDisabled;
    } else {
      return AppColors.whiteSkeleton;
    }
  }

  Widget _buttonIconDisplay() {
    if (buttonClockType == ButtonClockType.buttonClockin ||
        buttonClockType == ButtonClockType.buttonWillClockIn) {
      return Image.asset(
        Assets.iconClockinButton,
        color: AppColors.whiteBackground,
        height: SizeConfig.blockSizeHorizontal * 35,
        width: SizeConfig.blockSizeHorizontal * 35,
        fit: BoxFit.cover,
      );
    } else if (buttonClockType == ButtonClockType.buttonClockOut ||
        buttonClockType == ButtonClockType.buttonWillClockOut) {
      return Image.asset(
        Assets.iconClockoutButton,
        color: AppColors.whiteBackground,
        height: SizeConfig.blockSizeHorizontal * 35,
        width: SizeConfig.blockSizeHorizontal * 35,
        fit: BoxFit.cover,
      );
    } else if (buttonClockType == ButtonClockType.buttonDayOff) {
      return Image.asset(
        Assets.iconClockoutButton,
        color: AppColors.whiteBackground,
        height: SizeConfig.blockSizeHorizontal * 35,
        width: SizeConfig.blockSizeHorizontal * 35,
        fit: BoxFit.cover,
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  String _buttonTextDisplay() {
    if (buttonClockType == ButtonClockType.buttonClockin ||
        buttonClockType == ButtonClockType.buttonWillClockIn) {
      return "attendance_clock_in".tr.toUpperCase();
    } else if (buttonClockType == ButtonClockType.buttonClockOut ||
        buttonClockType == ButtonClockType.buttonWillClockOut) {
      // return "attendance_clock_out".tr.toUpperCase();
      return "CLOCK OUT";
    } else if (buttonClockType == ButtonClockType.buttonDayOff) {
      return "attendance_day_off".tr.toUpperCase();
    } else {
      return "";
    }
  }
}
