// Display current date and time
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AttendanceCurrentTimeDisplay extends StatelessWidget {
  // constructor
  const AttendanceCurrentTimeDisplay({required this.dateTime});

  final String dateTime;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _currentTime()),
        ResponsiveRowColumnItem(child: _currentDate())
      ],
    );
  }

  Widget _currentTime() {
    return RobotoTextView(
      // value: "12:00",
      value: DateFormat("HH:mm:ss").format(
        DateTime.parse(dateTime),
      ),
      color: AppColors.whiteBackground,
      size: SizeConfig.safeBlockHorizontal * 13,
      fontWeight: FontWeight.w500,
      alignText: AlignTextType.center,
    );
  }

  Widget _currentDate() {
    return RobotoTextView(
      value: "Saturday, 05 March 2022",
      // value: DateFormat("EEEE, dd MMM y").format(
      //   DateTime.parse(dateTime),
      // ),
      color: AppColors.whiteBackground,
      size: SizeConfig.safeBlockHorizontal * 4,
      fontWeight: FontWeight.w400,
      alignText: AlignTextType.center,
    );
  }
}
