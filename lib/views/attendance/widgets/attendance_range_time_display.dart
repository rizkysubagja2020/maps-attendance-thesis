import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_row_column.dart';

class AttendanceTimeRangeDisplay extends StatelessWidget {
  // constructor
  const AttendanceTimeRangeDisplay({
    required this.infoClockIn,
    required this.infoClockOut,
    required this.timeAdvice,
    required this.timeReminder,
    required this.timeMessage,
    // required this.todayAttendanceInfo,
  });

  final String infoClockIn;
  final String infoClockOut;
  final String timeAdvice;
  final String timeReminder;
  final dynamic timeMessage;
  // final dynamic todayAttendanceInfo;

  // String _todayCreateAt() {
  //   return getDateCreatedSheet(
  //     todayAttendanceInfo[0]["created_at"].toString(),
  //   );
  // }

  // String _todayClockInDateTime() {
  //   return clockInDateTimeModify(
  //     todayAttendanceInfo[0]["clockin"]["date_time"].toString(),
  //   );
  // }

  // String _todayClockOutDateTime() {
  //   return clockOutDateTimeModify(
  //     todayAttendanceInfo[0]["clockout"]["date_time"].toString(),
  //   );
  // }

  // String _todayClockInImage() {
  //   return todayAttendanceInfo[0]["clockin"]["images"]["original"].toString();
  // }

  // String _todayClockOutImage() {
  //   return todayAttendanceInfo[0]["clockout"]["images"]["original"].toString();
  // }

  // String _todayClockInNotes() {
  //   return todayAttendanceInfo[0]["clockin"]["note"].toString();
  // }

  // String _todayClockOutNotes() {
  //   return todayAttendanceInfo[0]["clockout"]["note"].toString();
  // }

  // LocationType _todayClockInLocation() {
  //   return clockInTypeModify(
  //     todayAttendanceInfo[0]["clockin"]["type"].toString(),
  //   );
  // }

  // LocationType _todayClockOutLocation() {
  //   return clockOutTypeModify(
  //     todayAttendanceInfo[0]["clockout"]["type"].toString(),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _timeAttention()),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.blockSizeVertical * 2),
        ),
        ResponsiveRowColumnItem(child: _timeWorkDuration()),
      ],
    );
  }

  Widget _timeAttention() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _timeAdviceDisplay()),
        ResponsiveRowColumnItem(child: _timeReminderDisplay()),
        ResponsiveRowColumnItem(child: _timeMessageDisplay()),
      ],
    );
  }

  Widget _timeAdviceDisplay() {
    if (timeAdvice.isNotEmpty) {
      final int _startWord = timeAdvice.indexOf("Minimum");
      final int _endWord = timeAdvice.indexOf("time");

      final String _getWordClockType = timeAdvice.substring(
        _startWord + "Minimum".length,
        _endWord,
      );

      final int _findWord = timeAdvice.indexOf("is");
      final String _resultRaw = timeAdvice.substring(_findWord);
      final String _getWordTime = _resultRaw.substring(2);

      if (timeAdvice == "Minimum${_getWordClockType}time is$_getWordTime") {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: GoogleFonts.roboto(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    color: AppColors.basicDark,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w300,
                  ),
                  children: <TextSpan>[
                    const TextSpan(text: "Minimum"),
                    TextSpan(
                      text: _getWordClockType,
                      style: const TextStyle(fontWeight: FontWeight.w700),
                    ),
                    const TextSpan(text: "time "),
                    const TextSpan(text: "is"),
                    TextSpan(
                      text: _getWordTime,
                      style: const TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
            ),
            ResponsiveRowColumnItem(
              child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
            ),
          ],
        );
      } else {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RobotoTextView(
                alignText: AlignTextType.center,
                value: timeAdvice,
                color: AppColors.basicDark,
                size: SizeConfig.safeBlockHorizontal * 4,
                fontWeight: FontWeight.w300,
              ),
            ),
            ResponsiveRowColumnItem(
              child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
            ),
          ],
        );
      }
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget _timeReminderDisplay() {
    // log("timeReminder = $timeReminder");
    if (timeReminder.isNotEmpty) {
      // if (timeReminder == "Available at$_getWordTime") {
      if (timeReminder.contains("Available")) {
        final int _findWord = timeReminder.indexOf("at");
        final String _resultRaw = timeReminder.substring(_findWord);
        final String _getWordTime = _resultRaw.substring(2);

        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: GoogleFonts.roboto(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    color: AppColors.basicDark,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w300,
                  ),
                  children: <TextSpan>[
                    const TextSpan(text: "Available "),
                    const TextSpan(text: "at"),
                    TextSpan(
                      text: _getWordTime,
                      style: const TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
            ),
            ResponsiveRowColumnItem(
              child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
            ),
          ],
        );
      } else if (timeReminder.contains("Tomorrow") &&
          timeReminder.contains("Saturday")) {
        final int _startWord = timeReminder.indexOf("is");
        final int _endWord = timeReminder.indexOf("Saturday");

        final String _getWeekEndWord = timeReminder.substring(
          _startWord + "is".length,
          _endWord,
        );

        final int _findWord = timeReminder.indexOf(_getWeekEndWord);
        final String _result = timeReminder.substring(_findWord);

        // log(timeReminder.substring(_startWord + "is".length, _endWord));
        // log(timeReminder.substring(_findWord));

        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: GoogleFonts.roboto(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    color: AppColors.basicDark,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w300,
                  ),
                  children: <TextSpan>[
                    const TextSpan(text: "Tomorrow "),
                    const TextSpan(text: "is"),
                    TextSpan(
                      text: _result,
                      style: const TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
            ),
            ResponsiveRowColumnItem(
              child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
            ),
          ],
        );
      } else {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RobotoTextView(
                alignText: AlignTextType.center,
                value: timeReminder,
                color: AppColors.basicDark,
                fontStyle: FontStyle.normal,
                size: SizeConfig.safeBlockHorizontal * 4,
                fontWeight: FontWeight.w700,
              ),
            ),
            ResponsiveRowColumnItem(
              child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
            ),
          ],
        );
      }
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget _timeMessageDisplay() {
    if (timeMessage.runtimeType == String) {
      if (timeMessage != "") {
        return RobotoTextView(
          value: timeMessage.toString(),
          color: AppColors.basicDark,
          size: SizeConfig.safeBlockHorizontal * 4,
          fontWeight: FontWeight.w300,
        );
      } else {
        return const SizedBox.shrink();
      }
    } else {
      /// get character from [timeMessage]
      final _result = timeMessage.toString();
      /// remove first and last character from [result]
      final _resultWord = _result.substring(1, _result.length - 1 );
      return RobotoTextView(
        value: _resultWord,
        color: AppColors.basicDark,
        alignText: AlignTextType.center,
        size: SizeConfig.safeBlockHorizontal * 4,
        fontWeight: FontWeight.w700,
      );
    }
  }

  Widget _timeWorkDurationParent({required Widget child}) {
    if (infoClockIn == "-" && infoClockOut == "-") {
      return child;
    } else {
      return Stack(
        children: <Widget>[
          child,
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: SizeConfig.blockSizeHorizontal * 12,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(SizeConfig.blockSizeHorizontal * 3),
              ),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.transparent,
                  onTap: () {
                    // Get.bottomSheet(
                    //   AttendanceWorkDayDetailSheet(
                    //     createdAt: _todayCreateAt(),
                    //     // date time
                    //     clockInDateTime: _todayClockInDateTime(),
                    //     clockOutDateTime: _todayClockOutDateTime(),
                    //     // image
                    //     clockInImage: _todayClockInImage(),
                    //     clockOutImage: _todayClockOutImage(),
                    //     // notes
                    //     clockInNotes: _todayClockInNotes(),
                    //     clockOutNotes: _todayClockOutNotes(),
                    //     // type
                    //     clockInType: _todayClockInLocation(),
                    //     clockOutType: _todayClockOutLocation(),
                    //   ),
                    //   persistent: false,
                    //   isScrollControlled: true,
                    // );
                  },
                  child: Ink(
                    color: Colors.transparent,
                    height: SizeConfig.blockSizeVertical * 5,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    }
  }

  Widget _timeWorkDuration() {
    return _timeWorkDurationParent(
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.blockSizeHorizontal * 12,
        ),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.ROW,
          rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: _timeClock(
                value: infoClockIn,
                type: AttendanceType.clockin,
              ),
            ),
            ResponsiveRowColumnItem(
              child: _timeClock(
                value: infoClockOut,
                type: AttendanceType.clockout,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _timeClock({required String value, required AttendanceType type}) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            type == AttendanceType.clockin
                ? Assets.iconClockinButton
                : Assets.iconClockoutButton,
            color: value == "-" ? AppColors.greyDisabled : AppColors.greenDark,
            width: SizeConfig.blockSizeHorizontal * 10,
            height: SizeConfig.blockSizeHorizontal * 10,
          ),
        ),
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnCrossAxisAlignment: value == "-"
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.start,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: RobotoTextView(
                  value: value,
                  color: value != "-"
                      ? AppColors.basicDark
                      : AppColors.greyDisabled,
                  size: SizeConfig.safeBlockHorizontal * 5,
                  fontWeight: FontWeight.w700,
                ),
              ),
              ResponsiveRowColumnItem(
                child: RobotoTextView(
                  value: type == AttendanceType.clockin
                      ? "attendance_clock_in".toUpperCase()
                      : "attendance_clock_out".toUpperCase(),
                  color: value != "-"
                      ? AppColors.basicDark
                      : AppColors.greyDisabled,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
