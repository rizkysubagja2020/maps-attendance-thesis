import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart' as getx;
import 'package:maps_attendance_thesis/controllers/login_controller.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/form/login_form.dart';
import 'package:maps_attendance_thesis/views/widgets/frame/frame_scaffold.dart';
import 'package:maps_attendance_thesis/views/widgets/shimmer/shimmer_placeholder.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:responsive_framework/responsive_value.dart';

class LoginPage extends StatelessWidget {
  // inject instance from controller
  final LoginController _loginController = getx.Get.put(LoginController());

  /// detect keyboard height and status
  /// [isSmall] are false then disable scroll physics
  ScrollPhysics? _isUseScroll(bool isSmall) {
    // log("is keyboard small = $isSmall");
    if (isSmall == false) {
      return const NeverScrollableScrollPhysics();
    } else {
      return null;
    }
  }

  /// detect keyboard is visible, [isKeyboardVisible] are true
  /// then show bezier curve
  Widget _isDisplayCurve(bool isKeyboardVisible, BuildContext context) {
    // log("screen height = ${SizeConfig.screenHeight} screen width = ${SizeConfig.screenWidth}");
    if (!isKeyboardVisible) {
      return _curve(context);
    } else {
      return const SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.greenDark,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: KeyboardSizeProvider(
        child: FrameScaffold(
          heightBar: 0,
          elevation: 0,
          avoidBottomInset: true,
          color: Platform.isIOS ? AppColors.whiteBackground : null,
          statusBarColor: AppColors.whiteBackground,
          statusBarIconBrightness: Brightness.dark,
          view: Container(
            color: AppColors.whiteBackground,
            child: Consumer<ScreenHeight>(
              builder: (context, _res, child) {
                return _mainGestureWrapper(context, _res);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _mainGestureWrapper(BuildContext context, ScreenHeight _res) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        if (_res.isSmall) {
          _loginController.resetScrollView();
        }
      },
      child: _mainViewWrapper(context, _res),
    );
  }

  /// wrap main view with responsive column, [_res] param
  /// from [Consumer] builder
  Widget _mainViewWrapper(BuildContext context, ScreenHeight _res) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Expanded(
            child: CustomScrollView(
              controller: _loginController.scrollController,
              physics: _isUseScroll(_res.isSmall),
              slivers: [_sliver()],
            ),
          ),
        ),
        ResponsiveRowColumnItem(
          child: ResponsiveVisibility(
            hiddenWhen: const [Condition.smallerThan(name: MOBILE)],
            child: _isDisplayCurve(_res.isOpen, context),
          ),
        ),
      ],
    );
  }

  /// Implement sliver widget with responsive column and items
  Widget _sliver() {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 4),
          ),
          ResponsiveRowColumnItem(child: _loginLabel()),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 1),
          ),
          ResponsiveRowColumnItem(child: _companyName()),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 3.5),
          ),
          ResponsiveRowColumnItem(
            child: LoginForm(loginController: _loginController),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 3),
          ),
          ResponsiveRowColumnItem(child: _footerContent()),
        ],
      ),
    );
  }

  /// bezier curve wrapper [_greenLightCurve] are behinded from [_greenDarkCurve]
  Widget _curve(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _greenLightCurve(context),
        _greenDarkCurve(context),
      ],
    );
  }

  Widget _greenLightCurve(BuildContext context) {
    return ClipPath(
      clipper: ProsteBezierCurve(
        reclip: false,
        position: ClipPosition.top,
        list: <BezierCurveSection>[
          BezierCurveSection(
            start: Offset(MediaQuery.of(context).size.width, 30),
            top: Offset(MediaQuery.of(context).size.width / 4 * 3, 10),
            end: Offset(MediaQuery.of(context).size.width / 2, 40),
          ),
          BezierCurveSection(
            start: Offset(MediaQuery.of(context).size.width / 2, 30),
            top: Offset(MediaQuery.of(context).size.width / 4, 60),
            end: Offset.zero,
          ),
        ],
      ),
      child: Container(
        height: SizeConfig.blockSizeVertical * 22,
        color: AppColors.greenLight,
      ),
    );
  }

  Widget _greenDarkCurve(BuildContext context) {
    return ClipPath(
      clipper: ProsteBezierCurve(
        reclip: false,
        position: ClipPosition.top,
        list: <BezierCurveSection>[
          BezierCurveSection(
            start: Offset(MediaQuery.of(context).size.width, 50),
            top: Offset(MediaQuery.of(context).size.width / 4 * 3, 100),
            end: Offset(MediaQuery.of(context).size.width / 2, 90),
          ),
          BezierCurveSection(
            start: Offset(MediaQuery.of(context).size.width / 2, -20),
            top: Offset(MediaQuery.of(context).size.width / 4, 50),
            end: const Offset(0, 120),
          ),
        ],
      ),
      child: Container(
        height: SizeConfig.blockSizeVertical * 22,
        color: AppColors.greenDark,
      ),
    );
  }

  Container _loginLabel() {
    return Container(
      margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 14),
      child: Align(
        alignment: Alignment.topLeft,
        child: RobotoTextView(
          value: "login_label".tr,
          size: SizeConfig.safeBlockHorizontal * 10,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  /// If [company] name length less that 1 or empty, then return [ShimmerPlaceholder],
  /// otherwise return dynamic name from API
  Widget _companyName() {
    return getx.Obx(() {
      if (_loginController.company.value.length > 1) {
        return Container(
          margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 14),
          child: Align(
            alignment: Alignment.topLeft,
            child: RobotoTextView(
              value: _loginController.company.value,
              size: SizeConfig.safeBlockHorizontal * 5,
              fontWeight: FontWeight.w400,
            ),
          ),
        );
      } else {
        return ShimmerPlaceholder(width: SizeConfig.blockSizeHorizontal * 60);
      }
    });
  }

  Widget _footerContent() {
    return Align(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              Assets.imageLogoApp,
              height: SizeConfig.blockSizeHorizontal * 15,
              width: SizeConfig.blockSizeHorizontal * 25,
              fit: BoxFit.cover,
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 1.5),
          ),
          ResponsiveRowColumnItem(child: _versionApp())
        ],
      ),
    );
  }

  Widget _versionApp() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: getx.Obx(
            () =>
                RobotoTextView(value: "Version ${_loginController.versionApp}"),
          ),
        ),
        ResponsiveRowColumnItem(
          child: getx.Obx(
            () => RobotoTextView(
              value:
                  "©${_loginController.year.value}. ${_loginController.appName.value}",
              alignText: AlignTextType.center,
            ),
          ),
        ),
      ],
    );
  }
}
