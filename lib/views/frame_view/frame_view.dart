import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/frame/frame_scaffold_bottom_nav.dart';
import 'package:maps_attendance_thesis/views/widgets/frame/frame_widget_controller.dart';

class FrameView extends StatelessWidget {
  FrameView({Key? key}) : super(key: key);

  final FrameController _controller = Get.put(FrameController());

 @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.whiteBackground,
        systemNavigationBarIconBrightness: Brightness.dark,
      ),
      child: Obx(
        () => FrameScaffoldBottomNavigation(
          isImplyLeading: false,
          elevationAppBar: 0,
          colorScaffold: _screenColor(),
          titleScreen: _screenName(),
          isCenter: _isTitleCenter(),
          action: _actionWidget(context),
          statusBarColor: AppColors.greenDark,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
        ),
      ),
    );
  }

  Color _screenColor() {
    if (_controller.defaultIndex.value == 0) {
      return AppColors.greenDark;
    } else if (_controller.defaultIndex.value == 3) {
      return AppColors.greenDark;
    } else {
      return AppColors.whiteBackground;
    }
  }

  String _screenName() {
    if (_controller.defaultIndex.value == 0) {
      return "${"label_hi".tr}, ${_controller.containName.value}";
    } else if (_controller.defaultIndex.value == 1) {
      return "Attendance History";
    } else if (_controller.defaultIndex.value == 2) {
      return "Calendar";
    } else {
      return "Profile";
    }
  }

  bool _isTitleCenter() {
    if (_controller.defaultIndex.value == 0) {
      return false;
    } else {
      return true;
    }
  }

  Widget _actionWidget(BuildContext context) {
    if (_controller.defaultIndex.value == 0) {
      return Container(
        margin: EdgeInsets.only(right: SizeConfig.blockSizeHorizontal * 3),
        child: Align(
          alignment: Alignment.centerRight,
          child: Image.asset(Assets.imageDRTWhite),
        ),
      );
    } else if (_controller.defaultIndex.value == 3) {
      return Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.blockSizeHorizontal * 2.5,
              ),
              child: Image.asset(
                Assets.iconLogout,
                color: AppColors.whiteBackground,
                width: SizeConfig.blockSizeHorizontal * 7.5,
                height: SizeConfig.blockSizeHorizontal * 7.5,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.only(
                right: SizeConfig.blockSizeHorizontal * 1,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  SizeConfig.blockSizeHorizontal * 10,
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    splashColor: Colors.transparent,
                    onTap: () {
                      Get.offAndToNamed("/login");
                      // showDialog(
                      //   context: context,
                      //   builder: (BuildContext context) {
                      //     // return LogoutDialog();
                      //   },
                      // );
                    },
                    child: Ink(
                      color: Colors.transparent,
                      width: SizeConfig.blockSizeHorizontal * 12,
                      height: SizeConfig.blockSizeHorizontal * 12,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}

