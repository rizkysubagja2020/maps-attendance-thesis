import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    Key? key,
    required this.controller,
    this.inputType,
    this.hintText,
    this.password,
    this.iconPrefix,
    this.changeIconSuffix,
    this.focusNode,
    this.errorText,
    this.autofillHint,
    this.labelText,
  }) : super(key: key);

  final TextEditingController controller;
  final TextInputType? inputType;
  final String? hintText;
  final bool? password;
  final IconData? iconPrefix;
  final bool? changeIconSuffix;
  final FocusNode? focusNode;
  final String? errorText;
  final Iterable<String>? autofillHint;
  final String? labelText;

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isShowPassword = true;

  dynamic onShowPassword() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.hintText ?? "",
            style: GoogleFonts.roboto(
              color: AppColors.basicDark,
              fontSize: SizeConfig.safeBlockHorizontal * 4,
              fontWeight: FontWeight.w300,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        SizedBox(height: SizeConfig.blockSizeVertical * 1.2),
        TextField(
          keyboardType: widget.inputType,
          controller: widget.controller,
          textAlign: TextAlign.left,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(SizeConfig.blockSizeVertical * 1.5),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            errorText: widget.errorText,
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.redPinPoint, width: 2),
            ),
            suffixIconConstraints: BoxConstraints(
              maxWidth: SizeConfig.blockSizeHorizontal * 9,
            ),
            suffixIcon: widget.changeIconSuffix != null
                ? Container(
                    decoration: BoxDecoration(
                      // color: Colors.red,
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          SizeConfig.blockSizeHorizontal * 3,
                        ),
                      ),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        onTap: onShowPassword,
                        child: Ink(
                          width: SizeConfig.blockSizeHorizontal * 10,
                          height: SizeConfig.blockSizeHorizontal * 12,
                          child: Icon(
                            isShowPassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: isShowPassword
                                ? AppColors.basicDark
                                : AppColors.greyDisabled,
                          ),
                        ),
                      ),
                    ),
                  )
                : null,
          ),
          obscureText:
              // ignore: avoid_bool_literals_in_conditional_expressions
              widget.changeIconSuffix != null ? isShowPassword : false,
          focusNode: widget.focusNode,
          autofillHints: widget.autofillHint,
          style: GoogleFonts.roboto(
            color: AppColors.basicDark,
            fontStyle: FontStyle.normal,
          ),
        ),
      ],
    );
  }
}
