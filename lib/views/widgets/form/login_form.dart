import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:maps_attendance_thesis/controllers/login_controller.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/buttons/flat_buttons.dart';
import 'package:maps_attendance_thesis/views/widgets/form/custom_text_field.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:get/get.dart';

/// [LoginForm] widget class with parameter [loginController] required
class LoginForm extends StatelessWidget {
  const LoginForm({
    Key? key,
    required this.loginController,
  }) : super(key: key);

  // parameter
  final LoginController loginController;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _formLogin()),
        ResponsiveRowColumnItem(child: _forgotPasswordLabelButton(context)),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.blockSizeVertical * 2),
        ),
        ResponsiveRowColumnItem(child: _loginButton(context)),
      ],
    );
  }

  Container _formLogin() {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.blockSizeHorizontal * 14,
      ),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: AutofillGroup(
        child: Column(
          children: <Widget>[
            _formloginNik(),
            const SizedBox(height: 20),
            _formloginPassword(),
          ],
        ),
      ),
    );
  }

  CustomTextField _formloginNik() {
    return CustomTextField(
      autofillHint: const [AutofillHints.username],
      controller: loginController.nikController,
      inputType: TextInputType.number,
      hintText: "login_form_nik".tr,
      iconPrefix: Icons.person,
      focusNode: loginController.nikFocusNode,
    );
  }

  CustomTextField _formloginPassword() {
    return CustomTextField(
      autofillHint: const [AutofillHints.password],
      controller: loginController.passwordController,
      hintText: "login_form_password".tr,
      changeIconSuffix: true,
      iconPrefix: Icons.lock,
      focusNode: loginController.passwordFocusNode,
    );
  }

  Container _loginButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.blockSizeHorizontal * 14,
      ),
      width: SizeConfig.screenWidth,
      child: Obx(
        () => CustomFlatButton(
          onTap: () => loginController.validateUserAction(),
          text: "login_label".tr.toUpperCase(),
          brightness: BrightnessType.dark,
          loading: loginController.isLoading.value,
        ),
      ),
    );
  }

  Widget _forgotPasswordLabelButton(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      color: Colors.transparent,
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.blockSizeHorizontal * 14,
      ),
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: GestureDetector(
        onTap: () => loginController.onPressForgotPassword(context),
        child: RobotoTextView(
          value: "login_label_forgot_password".tr,
          size: SizeConfig.safeBlockHorizontal * 4,
        ),
      ),
    );
  }
}
