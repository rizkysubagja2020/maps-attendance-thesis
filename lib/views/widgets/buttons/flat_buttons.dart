import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';

class CustomFlatButton extends StatelessWidget {
  const CustomFlatButton({
    Key? key,
    required this.onTap,
    required this.text,
    required this.brightness,
    this.textColor,
    this.height,
    this.width,
    this.image,
    this.loading = false,
  }) : super(key: key);

  final VoidCallback onTap;
  final String text;
  final BrightnessType brightness;
  final Color? textColor;
  final double? height;
  final double? width;
  final String? image;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ElevatedButton(
      onPressed: () {
        // ignore: unnecessary_statements
        loading ? null : onTap();
      },
      style: ElevatedButton.styleFrom(
        fixedSize: Size(
          width ?? SizeConfig.blockSizeHorizontal * 80,
          height ?? SizeConfig.blockSizeVertical * 5,
        ),
        primary: _colorSelection(),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            SizeConfig.blockSizeHorizontal * 3,
          ),
        ),
      ),
      child: _buttonContent(),
    );
  }

  Color _colorSelection() {
    if (loading) {
      return AppColors.greyDisabled;
    } else if (brightness == BrightnessType.danger) {
      return AppColors.redAlert;
    } else if (brightness == BrightnessType.optional) {
      return AppColors.whiteSkeleton;
    } else if (brightness == BrightnessType.light) {
      return Colors.white;
    } else if (brightness == BrightnessType.disable) {
      return AppColors.greyDisabled;
    } else {
      return AppColors.bluePrimary;
    }
  }

  Widget _buttonContent() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Builder(
          builder: (_) {
            if (image != null) {
              return Container(
                margin: EdgeInsets.only(
                  right: SizeConfig.blockSizeHorizontal * 2,
                ),
                child: Image.asset(
                  image!,
                  height: SizeConfig.blockSizeVertical * 4,
                ),
              );
            } else {
              return Container();
            }
          },
        ),
        Text(
          text,
          style: GoogleFonts.roboto(
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            color: brightness == BrightnessType.light ||
                    brightness == BrightnessType.optional
                ? AppColors.bluePrimary
                : Colors.white,
          ),
        ),
      ],
    );
  }
}
