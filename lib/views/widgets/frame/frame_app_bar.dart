import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';

class FrameAppBar extends StatelessWidget implements PreferredSizeWidget {
  // constructor
  const FrameAppBar({
    this.titleScreen,
    this.heightBar,
    this.color,
    this.isCenter,
    this.elevation,
    this.isUseLeading,
    this.onBack,
    this.customLeading,
    this.action,
    this.isImplyLeading,
    this.customTitle,
    // status bar
    this.statusBarColor,
    this.statusBarIconBrightness,
    this.statusBarBrightness,
  });

  // parameter
  final String? titleScreen;
  final double? heightBar;
  final Color? color;
  final bool? isCenter;
  final double? elevation;
  final bool? isUseLeading;
  final Function()? onBack;
  final Widget? customLeading;
  final Widget? action;
  final bool? isImplyLeading;
  final Widget? customTitle;

  // status bar
  final Color? statusBarColor;
  final Brightness? statusBarIconBrightness;
  final Brightness? statusBarBrightness;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AppBar(
      title: _titleScreen(),
      backgroundColor: color ?? AppColors.greenDark,
      centerTitle: isCenter ?? false,
      elevation: elevation,
      bottomOpacity: 0.0,
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarColor: statusBarColor ?? AppColors.greenDark,
        statusBarIconBrightness: statusBarIconBrightness ?? Brightness.light,
        // iOS
        statusBarBrightness: statusBarBrightness ?? Brightness.light,
      ),
      leading: isUseLeading == null || isUseLeading == false
          ? null
          : _leadingWrapper(),
      actions: <Widget>[action ?? const SizedBox.shrink()],
      automaticallyImplyLeading: _enableImplyLeading(),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(heightBar ?? kToolbarHeight);

  bool _enableImplyLeading() {
    if (isImplyLeading == true) {
      return true;
    } else if (isImplyLeading == false) {
      return false;
    } else {
      return true;
    }
  }

  Widget _titleScreen() {
    if (titleScreen == null && isImplyLeading == null) {
      return const SizedBox.shrink();
    } else if (titleScreen == null && isImplyLeading == false) {
      return customTitle ?? const Text("");
    } else {
      return RobotoTextView(
        value: titleScreen ?? "",
        fontWeight: FontWeight.bold,
        size: SizeConfig.safeBlockHorizontal * 5,
        color: AppColors.whiteBackground,
      );
    }
  }

  Widget _leadingWrapper() {
    if (customLeading == null) {
      return _backButton();
    } else {
      return customLeading!;
    }
  }

  Widget _backButton() {
    return IconButton(
      onPressed: () {
        onBack == null ? Get.back() : onBack!();
      },
      iconSize: 15,
      icon: Image.asset(
        Assets.iconBack,
        color: AppColors.whiteBackground,
      ),
    );
  }
}
