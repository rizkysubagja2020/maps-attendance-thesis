// import 'package:dos_mobile_apps/utils/adaptive_size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';

import 'frame_app_bar.dart';
import 'frame_widget_controller.dart';

class FrameScaffoldBottomNavigation extends StatelessWidget {
  final FrameController _controller = Get.put(FrameController());

  // constructor
  FrameScaffoldBottomNavigation({
    this.titleScreen,
    this.heightBar,
    this.color,
    this.isCenter,
    this.elevation,
    this.elevationAppBar,
    this.isUseLeading,
    this.onBack,
    this.customLeading,
    this.action,
    this.isImplyLeading,
    this.customTitle,
    this.colorScaffold,

    // system status bar
    this.statusBarColor,
    this.statusBarIconBrightness,
    this.statusBarBrightness,
  });

  final String? titleScreen;
  final double? heightBar;
  final Color? color;
  final bool? isCenter;
  final double? elevation;
  final double? elevationAppBar;
  final bool? isUseLeading;
  final dynamic Function()? onBack;
  final Widget? customLeading;
  final Widget? action;
  final bool? isImplyLeading;
  final Widget? customTitle;
  final Color? colorScaffold;

  // system status bar
  final Color? statusBarColor;
  final Brightness? statusBarIconBrightness;
  final Brightness? statusBarBrightness;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DefaultTabController(
      length: _controller.widgetViewList.length,
      child: _getBuilderBottomNav(),
    );
  }

  Obx _getBuilderBottomNav() {
    return Obx(
      () => Scaffold(
        backgroundColor: colorScaffold,
        appBar: FrameAppBar(
          titleScreen: titleScreen,
          heightBar: heightBar,
          color: color,
          elevation: elevationAppBar,
          isCenter: isCenter,
          isUseLeading: isUseLeading,
          onBack: onBack,
          customLeading: customLeading,
          action: action,
          isImplyLeading: isImplyLeading,
          customTitle: customTitle,
          statusBarColor: statusBarColor,
          statusBarIconBrightness: statusBarIconBrightness,
          statusBarBrightness: statusBarBrightness,
        ),
        body: _controller.widgetViewList[_controller.defaultIndex.value],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 12.0,
          backgroundColor: AppColors.whiteBackground,
          selectedItemColor: AppColors.greenDark,
          unselectedItemColor: AppColors.basicDark,
          elevation: 20.0,
          items: <BottomNavigationBarItem>[
            _bottomNavigationBarItem(
              asset: Assets.iconHome,
              label: 'Home',
              index: 0,
            ),
            _bottomNavigationBarItem(
              asset: Assets.iconHistory,
              label: 'History',
              index: 1,
            ),
            _bottomNavigationBarItem(
              asset: Assets.iconCalendar,
              label: 'Calendar',
              index: 2,
            ),
            _bottomNavigationBarItem(
              asset: Assets.iconUser,
              label: 'Profile',
              index: 3,
            ),
          ],
          currentIndex: _controller.defaultIndex.value,
          onTap: _controller.onTapNavigation,
        ),
      ),
    );
  }

  BottomNavigationBarItem _bottomNavigationBarItem({
    required String asset,
    required String label,
    required int index,
  }) {
    return BottomNavigationBarItem(
      icon: Image.asset(
        asset,
        width: SizeConfig.blockSizeHorizontal * 8,
        height: SizeConfig.blockSizeVertical * 4,
        fit: BoxFit.cover,
        color: _controller.defaultIndex.value == index
            ? AppColors.greenDark
            : AppColors.basicDark,
      ),
      label: label,
    );
  }
}
