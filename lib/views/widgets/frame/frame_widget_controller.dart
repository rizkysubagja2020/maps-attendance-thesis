import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
// import 'package:maps_attendance_thesis/helpers/shared_pref.dart';
import 'package:maps_attendance_thesis/views/attendance/attendance_page.dart';
import 'package:maps_attendance_thesis/views/calendar/calendar_page.dart';
import 'package:maps_attendance_thesis/views/history/history_page.dart';
import 'package:maps_attendance_thesis/views/profile/profile_page.dart';

class FrameController extends GetxController {
  RxInt defaultIndex = RxInt(0);
  Rx<String> containName = Rx("Rizky Subagja");
  List<Widget> widgetViewList = [
    AttendancePage(),
    HistoryPage(),
    CalendarPage(),
    ProfilePage(),
  ];

  @override
  void onInit() {
    _getUserName();
    super.onInit();
  }

  // function for change state value
  // ignore: use_setters_to_change_properties
  void onTapNavigation(int index) {
    defaultIndex.value = index;
    log("${defaultIndex.value}");
  }

  Future<String?> _getUserName() async {
    final SharedPref _prefs = SharedPref();
    final String? _getName = await _prefs.readUserProfileName();
    containName.value = _getName ?? "Rizky Subagja";
  }
}