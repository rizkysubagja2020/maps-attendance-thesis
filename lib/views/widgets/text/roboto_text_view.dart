import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';

class RobotoTextView extends StatelessWidget {
  // constructor
  const RobotoTextView({
    required this.value,
    this.color,
    this.size,
    this.fontStyle,
    this.fontWeight,
    this.alignText,
  });

  final String value;
  final Color? color;
  final double? size;
  final FontStyle? fontStyle;
  final FontWeight? fontWeight;
  final AlignTextType? alignText;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Text(
      value,
      style: GoogleFonts.roboto(
        color: color ?? AppColors.basicDark,
        fontSize: size ?? SizeConfig.safeBlockHorizontal * 3,
        fontStyle: fontStyle ?? FontStyle.normal,
        fontWeight: fontWeight,
      ),
      textAlign: alignText == AlignTextType.center
          ? TextAlign.center
          : alignText == AlignTextType.right
              ? TextAlign.right
              : TextAlign.left,
    );
  }
}
