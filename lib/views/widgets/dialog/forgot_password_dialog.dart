import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maps_attendance_thesis/utils/app_colors.dart';
import 'package:maps_attendance_thesis/utils/assets_list.dart';
import 'package:maps_attendance_thesis/utils/enums.dart';
import 'package:maps_attendance_thesis/utils/size_config.dart';
import 'package:maps_attendance_thesis/views/widgets/text/roboto_text_view.dart';
import 'package:responsive_framework/responsive_framework.dart';

class ForgotPasswordDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.blockSizeHorizontal * 6),
      ),
      backgroundColor: AppColors.whiteBackground,
      content: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(Assets.imageForgotPassword),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 1.5),
          ),
          ResponsiveRowColumnItem(
            child: RobotoTextView(
              value: "forgot_password_question_label".tr,
              alignText: AlignTextType.center,
              size: SizeConfig.safeBlockHorizontal * 4.5,
              fontWeight: FontWeight.w700,
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.blockSizeVertical * 0.5),
          ),
          ResponsiveRowColumnItem(
            child: RichText(
              text: TextSpan(
                style: GoogleFonts.roboto(
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                  color: AppColors.basicDark,
                  fontStyle: FontStyle.normal,
                ),
                children: <TextSpan>[
                  TextSpan(text: "forgot_password_message".tr),
                  TextSpan(
                    text: " ${"forgot_password_message_HR".tr}",
                    style: const TextStyle(fontWeight: FontWeight.w700),
                  ),
                  TextSpan(text: " ${"forgot_password_or".tr} "),
                  TextSpan(
                    text: "forgot_password_message_admin".tr,
                    style: const TextStyle(fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
