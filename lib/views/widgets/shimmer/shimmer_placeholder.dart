import 'package:flutter/material.dart';
import 'package:maps_attendance_thesis/utils/adaptive_size.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerPlaceholder extends StatelessWidget {
  const ShimmerPlaceholder({
    required this.width,
    this.height,
    this.borderRadius,
    Key? key,
  }) : super(key: key);

  final double width;
  final double? height;
  final double? borderRadius;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: (Colors.grey[300])!,
      highlightColor: (Colors.grey[100])!,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(
            borderRadius ?? ResponsiveSize.width(2.5),
          ),
        ),
        height: height ?? ResponsiveSize.height(2),
        width: width,
      ),
    );
  }
}
